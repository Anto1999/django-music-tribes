from .urls import *
from django.urls import path
from . import views

app_name="tribes"

urlpatterns=[
    path('',views.index, name="index"),
    path('create_tribe/',views.create_tribe, name="create_tribe"),
    path('<int:tribe_id>/create_playlist/',views.create_playlist, name="create_playlist"),
    path("<int:tribe_id>/",views.tribe,name="tribe"),
    path("<int:tribe_id>/<int:playlist_id>/playlist/",views.playlist,name="playlist"),
    path('<int:tribe_id>/join_tribe/',views.join_tribe, name="join_tribe"),
    path("<int:tribe_id>/<int:playlist_id>/like/",views.like,name="like"),
    path('<int:tribe_id>/leave_tribe/',views.leave_tribe, name="leave_tribe"),
    path('<int:tribe_id>/kick_user/<int:user_id>',views.kick_user, name="kick_user"),
    path("<int:tribe_id>/<int:playlist_id>/add_comment/",views.add_comment, name="add_comment"),
    path('delete_tribe/<int:tribe_id>/',views.delete_tribe, name="delete_tribe"),
    path('delete_playlist/<int:playlist_id>/',views.delete_playlist, name="delete_playlist"),
    path('add_song/<int:playlist_id>/',views.add_song, name="add_song"),
    path('add_like/<int:song_id>/<int:playlist_id>/',views.add_like, name="add_like"),
    path("<int:tribe_id>/<int:playlist_id>/like/<int:comment_id>",views.like, name="like"),
]
