from django.db import models
from django.contrib.auth.models import User
from django.db.models.fields.related import ForeignKey
from django.utils import timezone
# Create your models here.

class Tribe(models.Model):
    chieftain = models.ForeignKey(User, on_delete=models.CASCADE,related_name="chieftain")
    name = models.CharField(unique=True,max_length=50,blank=False)
    logourl = models.CharField(max_length=1000,
            default="https://www.pngitem.com/pimgs/m/180-1802939_tribe-gaming-logo-tribe-gaming-hd-png-download.png",
            blank=False)
    description = models.TextField(max_length=1000)
    genre = models.CharField(max_length=20,blank=False)
    members=models.ManyToManyField(User,blank=True)
    

    
    # def chieftain(self):
    #     return self.chieftain

    def created_by(self):
        return self.chieftain.user.username

    def __str__(self):
        return self.name

class Song(models.Model):
    #playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE,related_name="playlist")
    user=models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
    name=models.CharField(max_length=200)
    artist=models.CharField(max_length=200)
    urlsong=models.CharField(max_length=500,blank=True)
    likes=models.ManyToManyField(User,blank=True, related_name="likes")
    def total_likes(self):
        return self.likes.count()

    def __str__(self):
        return self.name    
    
class Playlist(models.Model):
    name=models.CharField(max_length=100)
    user=models.ForeignKey(User, on_delete=models.CASCADE)
    songs=models.ManyToManyField(Song)
    description = models.TextField(max_length=1000)
    tribe  = models.ForeignKey(Tribe,on_delete=models.CASCADE)

    def __str__(self):
        return self.name



class Comment(models.Model):
    playlist=models.ForeignKey(Playlist, blank=True, related_name="comments", on_delete=models.CASCADE)
    user=models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
    text=models.TextField()
    date=models.DateTimeField(default=timezone.now)
    #likes=models.ManyToManyField(User,blank=True, related_name="likes")

  
    def __str__(self):
        return self.user.username

    

