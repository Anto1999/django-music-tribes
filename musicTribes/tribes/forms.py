from django.db.models import fields
from .models import *
from django.forms import ModelForm

class TribeForm(ModelForm):
    class Meta:
        model = Tribe
        fields = ['name', 'description','genre', 'logourl']

class PlaylistForm(ModelForm):
    class Meta:
        model = Playlist
        fields = ['name','description']

class SongForm(ModelForm):
    class Meta:
        model = Song
        fields = ['name','artist','urlsong']

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields=['text']
        
        