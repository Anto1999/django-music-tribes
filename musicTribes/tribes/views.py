from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.urls import reverse
from .models import *
from .forms import *
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
# Create your views here.

def index(request):
    tribes = Tribe.objects.all()
    user=request.user
    chieftain_tribes = []
    member_tribes = []
    non_member_tribes = []
    for tribe in tribes :
        if user.id == tribe.chieftain.id:
            chieftain_tribes.append(tribe)
        elif user in tribe.members.all():
            member_tribes.append(tribe)
        else :
            non_member_tribes.append(tribe)
    return render(request,'tribes/tribes.html',context={'chieftain_tribes': chieftain_tribes,
                     'member_tribes':member_tribes, 'non_member_tribes':non_member_tribes})
@login_required
def tribe(request, tribe_id):
    user=request.user.profile
    user_tribes=user.tribes.all()

    user_in_tribe=0

    tribe = Tribe.objects.filter(id=tribe_id).first()
    members = tribe.members.all()
    chieftain_id = tribe.chieftain.id
    user_id = request.user.id
    user_is_admin = user.is_admin
    if user_id == chieftain_id :
        user_chieftain=1
    else :
        user_chieftain=0
    print(user_chieftain)
    if tribe in user_tribes : 
        user_in_tribe=1
    playlists = Playlist.objects.filter(tribe=tribe)
    context = {"tribe":tribe,"playlists":playlists,"user_in_tribe":user_in_tribe, "members":members , "user_chieftain": user_chieftain, "user_is_admin": user_is_admin}
    return render(request,'tribes/tribe.html',context)

@login_required
def create_tribe(request):
    user=request.user.profile
    form = TribeForm(request.POST)
    if form.is_valid():
        saved_tribe = form.save(commit=False)
        saved_tribe.chieftain = request.user
        saved_tribe.save()
        saved_tribe.members.add(request.user)
        user.tribes.add(saved_tribe.id)
        return HttpResponseRedirect(reverse('tribes:tribe',args=(saved_tribe.id,)))
    else:
        form=TribeForm()
    context = {'form':form}
    return render(request,'tribes/create_tribe.html',context)

@login_required
def delete_tribe(request, tribe_id):
    user=request.user.profile
    tribe = Tribe.objects.filter(id=tribe_id).first()
    context = {}
    if request.user.id == tribe.chieftain.id or user.is_admin == 1 :
        tribe.delete()
        deleted = 1
    else :
        deleted = 0
    context['deleted']=deleted
    return render(request,'tribes/delete_tribe.html',context)

@login_required
def delete_playlist(request, playlist_id):
    user=request.user.profile
    playlist = Playlist.objects.filter(id=playlist_id).first()
    tribe=playlist.tribe
    context = {}
    if request.user.id == tribe.chieftain.id or user.is_admin == 1 or user.id == playlist.user.id:
        playlist.delete()
        deleted = 1
    else :
        deleted = 0
    context['deleted']=deleted
    return render(request,'tribes/delete_playlist.html',context)

def like(request, tribe_id):
    comment=get_object_or_404(Comment, id=request.POST.get('comment_id'))
    comment.likes.add(request.user)
    return HttpResponseRedirect(reverse('tribes:tribe',args=(tribe_id,)))

def add_like(request, song_id, playlist_id):
    song=Song.objects.get(id=song_id)
    song.likes.add(request.user)
    playlist=Playlist.objects.get(id=playlist_id)
    tribe_id=playlist.tribe.id
    return HttpResponseRedirect(reverse('tribes:playlist',args=(tribe_id, playlist_id,)))

def add_song(request, playlist_id):
    form = SongForm(request.POST)
    if form.is_valid():
        saved_song=form.save(commit=False)
        saved_song.user = request.user
        saved_song.save()
        playlist=Playlist.objects.get(id=playlist_id)
        tribe_id=playlist.tribe.id
        playlist.songs.add(saved_song)
        playlist.save()
        return HttpResponseRedirect(reverse('tribes:playlist',args=(tribe_id, playlist_id,)))
    else:
        form=SongForm()
    context = {'form':form}
    return render(request,'tribes/add_song.html',context)

        

@login_required
def create_playlist(request, tribe_id):
    form = PlaylistForm(request.POST)
    if form.is_valid():
        saved_playlist = form.save(commit=False)
        saved_playlist.user = request.user
        tribe=Tribe.objects.get(id=tribe_id)
        saved_playlist.tribe=tribe
        saved_playlist.save()
        return HttpResponseRedirect(reverse('tribes:tribe',args=(tribe_id,)))
    else:
        form=PlaylistForm()
    context = {'form':form}
    return render(request,'tribes/create_playlist.html',context)

def playlist(request, tribe_id, playlist_id):
    user=request.user.profile
    user_tribes=user.tribes.all()
    is_admin = user.is_admin
    tribe = Tribe.objects.get(id=tribe_id)
    if tribe in user_tribes:
        is_member=1
    else:
        is_member=0
    playlist = Playlist.objects.filter(tribe=tribe,id=playlist_id).first()
    songs = playlist.songs.all()
    songs_with_likes = []
    for song in songs:
        if request.user in song.likes.all():
            liked=1
        else:
            liked=0
        songs_with_likes.append({'song':song, 'liked':liked, 'number_of_likes':song.total_likes})
    if playlist.tribe.chieftain.id == request.user.id:
        is_chieftain=1
    else:
        is_chieftain=0
    if playlist.user.id == request.user.id:
        is_author = 1
    else:
        is_author=0
    context={"tribe":tribe, "playlist":playlist,"songs_with_likes":songs_with_likes,"is_chieftain":is_chieftain,
                 "is_admin":is_admin, "is_author":is_author, "is_member":is_member}
    return render(request,"tribes/playlist.html",context)

def join_tribe(request, tribe_id):
    tribe=Tribe.objects.get(id=tribe_id)
    tribe.members.add(request.user)
    tribe.save()
    request.user.profile.tribes.add(tribe)
    return redirect(("/"+str(tribe_id)+"/"))

def leave_tribe(request, tribe_id):
    tribe=Tribe.objects.get(id=tribe_id)
    tribe.members.remove(request.user)
    tribe.save()
    request.user.profile.tribes.remove(tribe)
    return redirect(("/"+str(tribe_id)+"/"))

def kick_user(request, user_id , tribe_id):
    tribe=Tribe.objects.get(id=tribe_id)
    kicked_user = User.objects.filter(id=user_id).first()
    tribe.members.remove(request.user)
    tribe.save()
    request.user.profile.tribes.remove(tribe)
    return redirect(("/"+str(tribe_id)+"/"))
    
@login_required
def add_comment(request, playlist_id,tribe_id):
    form = CommentForm(request.POST)
    if form.is_valid():
        saved_comment = form.save(commit=False)
        saved_comment.user = request.user
        playlist=Playlist.objects.get(id=playlist_id)
        saved_comment.playlist=playlist
        saved_comment.save()
        
        
        return HttpResponseRedirect(reverse('tribes:tribe',args=(tribe_id,)))
    else:
        form=CommentForm()
    context = {'add_comment':form}
    return render(request,'tribes/add_comment.html',context)
    

    
   












        


