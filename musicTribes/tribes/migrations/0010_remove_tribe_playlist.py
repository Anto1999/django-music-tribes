# Generated by Django 3.2.3 on 2021-06-01 19:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tribes', '0009_tribe_playlist'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tribe',
            name='playlist',
        ),
    ]
