# Generated by Django 3.2.3 on 2021-06-01 20:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tribes', '0013_alter_tribe_playlist'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tribe',
            name='playlist',
        ),
        migrations.AddField(
            model_name='playlist',
            name='tribe',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='tribes.tribe'),
            preserve_default=False,
        ),
    ]
