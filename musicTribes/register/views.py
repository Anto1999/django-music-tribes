from django.contrib.auth import logout, login, authenticate
from django.shortcuts import render, redirect
from .forms import *
from django.http.response import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth import update_session_auth_hash


# Create your views here.
def homepage(request):
	return render(request=request, template_name="tribes/base.html", context={})

def register(response):
    if response.method == "POST":
	    form = RegisterForm(response.POST)
	    if form.is_valid():
	        form.save()

	        return redirect("/")
    else:
	    form = RegisterForm()

    return render(response, "registration/register.html", {"form":form})

def login_request(request):
	if request.method == "POST":
		form = AuthenticationForm(request, data=request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user is not None:
				login(request, user)
				messages.info(request, f"You are now logged in as {username}.")
				return redirect("tribes:index")
			else:
				messages.error(request,"Invalid username or password.")
		else:
			messages.error(request,"Invalid username or password.")
	form = AuthenticationForm()
	return render(request=request, template_name="registration/login.html", context={"login_form":form})

def logout_request(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect("register:login")


def update_profile(request): 
    if request.method == "POST":
        form=UserUpdateForm(request.POST ,instance=request.user)
        form2=UserProfileUpdateForm(request.POST, instance=request.user.profile)
        if form.is_valid() and form2.is_valid():
	        form.save()
	        form2.save()
	        messages.info(request, "Updated successfully!")
	        return redirect("tribes:index")
    else:
        form=UserUpdateForm(instance=request.user)
        form2=UserProfileUpdateForm(instance=request.user.profile)
    return render(request=request, template_name='registration/update_profile.html', context={'update_form': form, 'update_form2' : form2})

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('tribes:index')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {
        'form': form
    })



