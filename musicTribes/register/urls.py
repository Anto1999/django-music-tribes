from django.contrib import admin
from django.shortcuts import redirect
from django.urls import path, include
from . import views as v
from django.contrib.auth import views as auth_views


app_name="register"

urlpatterns = [
    path('', v.homepage, name="homepage"),
    path('register/', v.register, name="register"),
    path('login/',v.login_request, name="login"),
    path('logout/',v.logout_request, name="logout"),
    path('update_profile/',v.update_profile, name="update_profile"),
    path('change_password/',v.change_password, name="change_password")
]