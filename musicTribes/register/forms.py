from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import UserProfile
from django.forms.widgets import PasswordInput
from django.shortcuts import get_object_or_404
from django.forms import ModelForm
from django.shortcuts import render, redirect
from django.http.response import HttpResponse

class RegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
	    model = User
	    fields = ["username", "email", "password1", "password2"]

class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
	    model = User
	    fields = ["username", "email"]

class UserProfileUpdateForm(forms.ModelForm):


    class Meta:
	    model = UserProfile
	    fields = ["first_name", "last_name", "adress", "city","logo"]