from django.contrib import admin
from django.db import models
from django.db.models.signals import pre_delete, pre_save
from django.dispatch.dispatcher import receiver
from django.contrib.auth.models import User
from tribes.models import Tribe
from django.core.exceptions import PermissionDenied
from django.contrib.auth.admin import UserAdmin
import random
import string

# Create your models here.
class UserProfile(models.Model):
	user = models.OneToOneField(User,on_delete=models.CASCADE)
	first_name= models.CharField(max_length=120, blank=True,)
	last_name = models.CharField(max_length=120, blank=True, )
	adress = models.CharField(max_length=220, blank=True,)
	city = models.CharField(max_length=220, blank=True, )
	state = models.CharField(max_length=220, blank=True, )
	country = models.CharField(max_length=120, blank=True, )
	tribes=models.ManyToManyField(Tribe,blank=True)
	logo = models.CharField(max_length=1000,
            default="https://www.ictbusiness.info/app/resize.php?src=http://www.ictbusiness.info/media/internet/facebook-default-profile-picture.jpg&w=670&q=80",
            blank=False)
	paid=models.DateField(auto_now_add=True)

	is_admin = models.IntegerField(default=0)
	def __str__(self):
		return(self.user.username)


class GuestUsername(models.Model):
    username   =models.CharField(max_length=120, blank=True,)
    active     =models.BooleanField(default=True)
    update     =models.DateTimeField(auto_now=True)
    timestamp  =models.DateTimeField(auto_now_add=True)
    def str(self):
        return(self.username.username)
def randomString(stringLength):
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))

def get_guestUser(user):
        if user.is_anonymous:
            random_username = f"{randomString(10)}_guest"
            guest_user = User.objects.create(username=random_username, is_active=False) 
            guest_user = GuestUsername.objects.create(user=guest_user, department="none")
            return guest_user
        else:
            return GuestUsername.objects.get(user=user)
        
  


@receiver(pre_delete, sender=User)
def delete_user(sender, instance, **kwargs):
    if instance.is_superuser:
        raise PermissionDenied	




User.profile=property(lambda u:UserProfile.objects.get_or_create(user=u)[0])
