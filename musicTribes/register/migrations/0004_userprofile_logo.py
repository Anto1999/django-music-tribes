# Generated by Django 3.2.3 on 2021-09-19 14:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0003_userprofile_is_admin'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='logo',
            field=models.CharField(default='https://www.ictbusiness.info/app/resize.php?src=http://www.ictbusiness.info/media/internet/facebook-default-profile-picture.jpg&w=670&q=80', max_length=1000),
        ),
    ]
